import { RoomUser } from "../game/game-rooms";
import { compose } from "../util/compose";
import { firstLetterToLowerCase } from "../util/first-letter-to-lower-case";
import { partial } from "../util/partial";
import { CarsRepository } from "./repositories/cars";
import { InterestingFactsRepository } from "./repositories/interesting-facts";

export class Commentator {
  private interestingFactsRepository = new InterestingFactsRepository();
  private carsRepository = new CarsRepository();

  public greet() {
    return `На вулиці зараз трохи пасмурно, але на Львів Арена зараз просто чудова атмосфера: двигуни гарчать, глядачі посміхаються а гонщики ледь помітно нервують та готуюуть своїх залізних конів до заїзду. А коментувати це все дійство Вам буду я, Ескейп Ентерович і я радий вас вітати зі словами Доброго Вам дня панове!`;
  }

  public announcePlayers(players: RoomUser[]): string {
    const beginning = `А тим часом список гонщиків: `;
    const announcePlayer = (player: RoomUser, i: number) =>
      `${player.name} під номером ${i + 1} на своїй ${this.carsRepository.getRandomOne()},`;
    const announcePlayers = players.map(announcePlayer).join(" ");

    const concat = (strings: string, string: string) => string.concat(strings);
    const concatAnnouncePlayers = partial(concat, announcePlayers);
    const replaceLast = (search: string, replace: string, string: string) => {
      const splitted = string.split("");
      splitted.splice(string.lastIndexOf(search), 1, replace);
      return splitted.join("");
    };
    const replaceLastCommaWithDot = partial(replaceLast, ",", ".");

    return compose(concatAnnouncePlayers, replaceLastCommaWithDot)(beginning);
  }

  public currentState(players: RoomUser[], secondsLeftUntilFinish: number) {
    const beginning = "Теперішній стан заїзду: ";
    const playerProgress = (player: RoomUser, i: number) => `${player.name} на ${i + 1} місці з прогресом ${Math.ceil(player.progress)}%,`;
    const playersProgress = players.map(playerProgress).join(" ");
    const leftUntilFinish = `${secondsLeftUntilFinish} секунд залишилось до кінця перегонів.`;

    const concat = (strings: string, string: string) => string.concat(strings);
    const concatWithPlayersProgress = partial(concat, playersProgress);
    const concatWithNewLine = partial(concat, "\n");
    const concatWithLeftUntilFinish = partial(concat, leftUntilFinish);
    const replaceLast = (search: string, replace: string, string: string) => {
      const splitted = string.split("");
      splitted.splice(string.lastIndexOf(search), 1, replace);
      return splitted.join("");
    };
    const replaceLastCommaWithDot = partial(replaceLast, ",", ".");

    return compose(concatWithPlayersProgress, replaceLastCommaWithDot, concatWithNewLine, concatWithLeftUntilFinish)(beginning);
  }

  public closeToFinish(player: RoomUser, charactersLeftUntilFinish: number) {
    return `${player.name} за ${charactersLeftUntilFinish} символів до фінішу з прогресом в ${Math.ceil(player.progress)}%.`;
  }

  public playerFinished(player: RoomUser) {
    return `${player.name} тільки що фінішував. Вітання!.`;
  }

  public announceTopThreePlayersResults(players: RoomUser[], gameStartedAt: Date) {
    const topThreePlayers = players.slice(0, 3);
    const finishedIn = (player: RoomUser) => Math.ceil((player.finishedAt - gameStartedAt.getTime()) / 1000);
    const announceWinner = (player: RoomUser, i: number) =>
      ` ${player.name} зайняв ${i + 1} місце ${player.finishedAt ? `, фінішувавши за ${finishedIn(player)} секунди` : ""}.`;

    return `Отже, заїзд завершився.`.concat(topThreePlayers.map(announceWinner).join(" "));
  }

  public interestingFact() {
    const randomInterestingFact = this.interestingFactsRepository.getRandomOne();
    return `Цікавий факт: ${firstLetterToLowerCase(randomInterestingFact)}`;
  }
}
