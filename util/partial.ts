export const partial =
  (fn, ...x) =>
  (...args) =>
    fn(...x, ...args);
