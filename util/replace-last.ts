export const replaceLast = (string: string, searchValue: string, replaceValue: string) => {
  const fIndex = string.lastIndexOf(searchValue);
  if (!fIndex) return string;

  return string.split("").splice(fIndex, 1, replaceValue).join("");
};
