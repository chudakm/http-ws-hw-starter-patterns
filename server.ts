import express, { Request, Response } from "express";
import http from "http";
import { Server } from "socket.io";
import socketHandler from "./socket";
import routes from "./routes";
import { STATIC_PATH, PORT, HTML_FILES_PATH } from "./config";

const app = express();
const httpServer = new http.Server(app);
const io = new Server(httpServer, { transports: ["websocket"] });

app.use("/", express.static(STATIC_PATH));
routes(app);

app.get("*", (req: Request, res: Response) => {
  res.redirect("/login");
});

socketHandler(io);

httpServer.listen(PORT, () => {
  console.log(`Listen server on port ${PORT}`);
});

export { app, httpServer };
