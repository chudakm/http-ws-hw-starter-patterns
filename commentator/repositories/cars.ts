import { cars } from "../../data";
import { getRandomInt } from "../../util/get-random-int";

export class CarsRepository {
  public getRandomOne() {
    const randomInt = getRandomInt(0, cars.length - 1);
    return cars[randomInt];
  }
}
