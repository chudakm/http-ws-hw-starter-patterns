import { MAXIMUM_USERS_FOR_ONE_ROOM } from "../socket/config";

export interface RoomUser {
  name: string;
  isReady: boolean;
  inputtedText: string;
  textToInput?: string;
  progress: number;
  finishedAt?: number;
}

export class GameRooms {
  private rooms: Array<{
    name: string;
    users: RoomUser[];
    isStarted: boolean;
    startedAt?: Date;
    text?: string;
  }> = [];

  public getAvailableRooms() {
    return this.rooms.filter(room => {
      const isMaximUsers = room.users.length === MAXIMUM_USERS_FOR_ONE_ROOM;
      const isGameStarted = room.isStarted;

      if (isGameStarted) return false;
      else if (isMaximUsers) return false;
      else return true;
    });
  }

  public getRoomByName(name: string) {
    return this.rooms.find(room => room.name === name);
  }

  public getRoomUsers(name: string) {
    const room = this.getRoomByName(name);
    if (!room) return [];
    else return room.users;
  }

  public getRoomByUsername(username: string) {
    return this.rooms.find(room => room.users.some(user => user.name === username));
  }

  public create(name: string, username: string) {
    if (this.rooms.some(room => room.name === name)) throw new Error(`ROOM_ALREADY_EXIST`);

    this.rooms.push({
      name,
      users: [{ name: username, isReady: false, progress: 0, inputtedText: "" }],
      isStarted: false,
    });
  }

  public join(name: string, username: string) {
    const room = this.rooms.find(room => room.name === name);
    if (!room) throw new Error(`Room ${name} doesnt exist.`);
    else if (room.users.length === MAXIMUM_USERS_FOR_ONE_ROOM) throw new Error(`MAXIMUM_USERS_FOR_ONE_ROOM`);

    room.users.push({ name: username, isReady: false, progress: 0, inputtedText: "" });
  }

  public disconnect(username: string) {
    const roomIndex = this.rooms.findIndex(room => room.users.some(user => user.name === username));
    if (roomIndex === -1) return;

    const room = this.rooms[roomIndex];
    const userIndex = room.users.findIndex(user => user.name === username);
    room.users.splice(userIndex, 1);

    if (!room.users.length) {
      this.rooms.splice(roomIndex, 1);
    }

    return room.name;
  }

  public isAllReadyInRoom(roomName: string) {
    const room = this.rooms.find(room => room.name === roomName);
    if (!room) return false;
    return room.users.every(user => user.isReady);
  }

  public isAllFinishedInRoom(roomName: string) {
    const room = this.rooms.find(room => room.name === roomName);
    if (!room) return false;

    return room.users.every(user => user.progress >= 100);
  }

  public getRoomUsersInRankingOrder(roomName: string) {
    const room = this.rooms.find(room => room.name === roomName);
    if (!room) return [];

    const finishedUsers = room.users.filter(user => user.finishedAt);
    const notFinishedUsers = room.users.filter(user => !user.finishedAt);

    // @ts-ignore
    finishedUsers.sort((a, b) => a.finishedAt - b.finishedAt);
    notFinishedUsers.sort((a, b) => b.progress - a.progress);

    return finishedUsers.concat(notFinishedUsers);
  }
}
