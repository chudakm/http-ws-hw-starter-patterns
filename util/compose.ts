export const compose =
  (...funcs) =>
  (...args) =>
    funcs.reduce((args, fn) => [fn(...args)], args);
