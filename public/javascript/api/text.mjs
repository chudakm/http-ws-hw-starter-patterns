export class GameApi {
  static async getText(id) {
    const response = await fetch(`/game/texts/${id}`);
    return response.json();
  }
}
