import { Namespace } from "socket.io";
import { SECONDS_TIMER_BEFORE_START_GAME } from "../socket/config";
import { sleep } from "../util/sleep";

export const startTimer = async (io: Namespace, roomName: string) => {
  for (let i = SECONDS_TIMER_BEFORE_START_GAME; i--; i > 0) {
    io.to(roomName).emit("UPDATE_TIMER", i);

    await sleep(1000);
  }
};
