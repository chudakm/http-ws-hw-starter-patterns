import { Namespace, Socket } from "socket.io";
import { Commentator } from "../../commentator/commentator";
import { texts } from "../../data";
import { GameRooms } from "../../game/game-rooms";
import { GameTimers } from "../../game/game-timers";
import { startTimer } from "../../game/timer";
import { getRandomInt } from "../../util/get-random-int";
import { MAXIMUM_USERS_FOR_ONE_ROOM } from "../config";

// facade
export class GameService {
  private connectedUsernames: string[] = [];
  private rooms = new GameRooms();
  private gameTimers = new GameTimers();
  private commentator = new Commentator();

  constructor(private io: Namespace) {}

  public onConnect(socket: Socket) {
    const username: string = (socket.handshake.query?.username as string) || "";
    if (!username || this.connectedUsernames.find(name => name === username)) {
      socket.emit("USERNAME_ALREADY_TAKEN");
      socket.disconnect();

      return;
    }

    this.connectedUsernames.push(username);
    socket.emit("UPDATE_ROOMS", this.rooms.getAvailableRooms());

    return username;
  }

  public onCreateRoom(socket: Socket, username: string, roomName: string, ack: (response?: string) => void) {
    if (!roomName) return ack("INVALID_ROOM_NAME");

    try {
      this.rooms.create(roomName, username);
      socket.join(roomName);

      this.io.emit("UPDATE_ROOMS", this.rooms.getAvailableRooms());
      this.io.to(roomName).emit("UPDATE_USERS", this.rooms.getRoomUsers(roomName));
      this.io.to(roomName).emit("UPDATE_COMMENTATOR_PHRASE", this.commentator.greet());
    } catch (err) {
      ack(err.message);
    }

    ack();
  }

  public onJoinRoom(socket: Socket, username: string, roomName: string, ack: (response?: string) => void) {
    try {
      this.rooms.join(roomName, username);
      socket.join(roomName);

      this.io.emit("UPDATE_ROOMS", this.rooms.getAvailableRooms());
      this.io.to(roomName).emit("UPDATE_USERS", this.rooms.getRoomUsers(roomName));
      this.io.to(roomName).emit("UPDATE_COMMENTATOR_PHRASE", this.commentator.greet());
    } catch (err) {
      ack(err.message);
    }

    ack();
  }

  public onLeaveRoom(socket: Socket, username: string, ack: (response?: string) => void) {
    try {
      const roomName = this.rooms.disconnect(username);

      this.io.emit("UPDATE_ROOMS", this.rooms.getAvailableRooms());

      if (roomName) {
        socket.leave(roomName);
        this.io.to(roomName).emit("UPDATE_USERS", this.rooms.getRoomUsers(roomName));

        if (this.rooms.isAllReadyInRoom(roomName)) {
          this.startGame(roomName);
        }
      }
    } catch (err) {
      ack(err.message);
    }

    ack();
  }

  public onUpdateReady(username: string, isReady: boolean) {
    const room = this.rooms.getRoomByUsername(username);
    if (room) {
      const user = room.users.find(user => user.name === username);
      if (user) {
        user.isReady = isReady;
      }

      this.io.to(room.name).emit("UPDATE_USERS", this.rooms.getRoomUsers(room.name));

      if (this.rooms.isAllReadyInRoom(room.name)) {
        this.startGame(room.name);
      }
    }
  }

  public onDisconnect(username: string) {
    const index = this.connectedUsernames.findIndex(name => name === username);
    if (index !== -1) this.connectedUsernames.splice(index, 1);

    const roomName = this.rooms.disconnect(username);
    if (roomName) {
      const room = this.rooms.getRoomByName(roomName);
      if (room?.users.length) {
        this.io.to(roomName).emit("UPDATE_USERS", room.users);
      }

      if (this.rooms.isAllReadyInRoom(roomName)) {
        this.startGame(roomName);
      }

      if (room?.isStarted && this.rooms.isAllFinishedInRoom(room?.name)) {
        this.endGame(room.name);
      }
    }
  }

  public onKeyPress(username: string, key: string, ack: (response: { inputtedText: string; textToInput: string }) => void) {
    const room = this.rooms.getRoomByUsername(username);
    if (room) {
      const user = room.users.find(user => user.name === username);
      if (user && user?.textToInput) {
        if (user.textToInput[0] === key) {
          user.textToInput = user.textToInput.substring(1);
          user.inputtedText += key;
          // @ts-ignore
          user.progress = ((room.text.length - user.textToInput.length) / room.text.length) * 100;
          if (!user.textToInput) {
            user.finishedAt = Date.now();

            this.io.to(room.name).emit("UPDATE_COMMENTATOR_PHRASE", this.commentator.playerFinished(user));
          }

          this.io.to(room.name).emit("UPDATE_USERS", room.users);

          if (user.textToInput.length === 30) {
            this.io.to(room.name).emit("UPDATE_COMMENTATOR_PHRASE", this.commentator.closeToFinish(user, 30));
          }
        }

        ack({ inputtedText: user.inputtedText, textToInput: user.textToInput });

        if (this.rooms.isAllFinishedInRoom(room.name)) {
          this.endGame(room.name);
        }
      }
    }
  }

  private startGame(roomName: string) {
    const room = this.rooms.getRoomByName(roomName);
    if (room && !room?.isStarted) {
      room.isStarted = true;
      room.startedAt = new Date();

      this.io.emit("UPDATE_ROOMS", this.rooms.getAvailableRooms());

      const textId = getRandomInt(0, texts.length);
      this.io.to(roomName).emit("UPDATE_TEXT_ID", textId);

      room.text = texts[textId];
      room.users.forEach(user => (user.textToInput = texts[textId]));

      startTimer(this.io, roomName).then(() => {
        this.io.to(roomName).emit("GAME_START");
        this.io.to(roomName).emit("UPDATE_COMMENTATOR_PHRASE", this.commentator.announcePlayers(room.users));

        this.gameTimers
          .start(this.io, roomName, (secondsElapsed, secondsUntilEnd) => {
            const isMultipleOf30 = secondsElapsed % 30 === 0;
            const isMultipleOf5 = secondsElapsed % 5 === 0;

            if (isMultipleOf30) {
              this.io.to(roomName).emit("UPDATE_COMMENTATOR_PHRASE", this.commentator.currentState(room.users, secondsUntilEnd));
            } else if (isMultipleOf5) {
              this.io.to(roomName).emit("UPDATE_COMMENTATOR_PHRASE", this.commentator.interestingFact());
            }
          })
          .then(() => {
            this.endGame(roomName);
          })
          .catch(() => {});
      });
    }
  }

  private endGame(roomName: string) {
    this.gameTimers.stop(roomName);

    const room = this.rooms.getRoomByName(roomName);
    if (!room) return;

    this.io.to(roomName).emit("GAME_END", this.commentator.announceTopThreePlayersResults(room.users, room.startedAt));

    room.startedAt = null;
    room.isStarted = false;
    room.text = "";
    room.users.forEach(user => {
      user.finishedAt = undefined;
      user.inputtedText = "";
      user.isReady = false;
      user.progress = 0;
      user.textToInput = undefined;
    });

    this.io.to(roomName).emit("UPDATE_USERS", room.users);
    if (room.users.length < MAXIMUM_USERS_FOR_ONE_ROOM) this.io.emit("UPDATE_ROOMS", this.rooms.getAvailableRooms());
  }
}

// factory
export function createGameService(io: Namespace) {
  return new GameService(io);
}
