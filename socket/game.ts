import { Namespace } from "socket.io";
import { createGameService } from "./services/game";

export default (io: Namespace) => {
  const gameService = createGameService(io);

  io.on("connection", socket => {
    const username = gameService.onConnect(socket);

    if (username) {
      socket.on("CREATE_ROOM", (roomName: string, ack: (response?: string) => void) => {
        gameService.onCreateRoom(socket, username, roomName, ack);
      });

      socket.on("JOIN_ROOM", (roomName: string, ack: (response?: string) => void) => {
        gameService.onJoinRoom(socket, username, roomName, ack);
      });

      socket.on("LEAVE_ROOM", (ack: (response?: string) => void) => {
        gameService.onLeaveRoom(socket, username, ack);
      });

      socket.on("UPDATE_READY", (isReady: boolean) => {
        gameService.onUpdateReady(username, isReady);
      });

      socket.on("KEYPRESS", (key: string, ack: (response: { inputtedText: string; textToInput: string }) => void) => {
        gameService.onKeyPress(username, key, ack);
      });

      socket.on("disconnect", () => {
        gameService.onDisconnect(username);
      });
    }
  });
};
