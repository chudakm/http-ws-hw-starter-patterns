export const firstLetterToLowerCase = (s: string) => s.charAt(0).toLowerCase() + s.slice(1);
