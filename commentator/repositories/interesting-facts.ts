import { interestingFacts } from "../../data";
import { getRandomInt } from "../../util/get-random-int";

export class InterestingFactsRepository {
  public getRandomOne() {
    const randomInt = getRandomInt(0, interestingFacts.length - 1);
    return interestingFacts[randomInt];
  }
}
