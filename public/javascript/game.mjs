import { GameApi } from "./api/text.mjs";
import { removeTimer } from "./domHelpers/game.mjs";
import { updateTimeLeftTimerText } from "./domHelpers/game.mjs";
import { renderWinnersModal } from "./domHelpers/game.mjs";
import { removeWinnersModal } from "./domHelpers/game.mjs";
import { updateComment } from "./domHelpers/game.mjs";
import { renderBackToRoomsButton } from "./domHelpers/game.mjs";
import { removeTextContainer } from "./domHelpers/game.mjs";
import { renderTextContainer } from "./domHelpers/game.mjs";
import { renderTimer } from "./domHelpers/game.mjs";
import { removeBackToRoomsButton } from "./domHelpers/game.mjs";
import {
  renderRooms,
  changePageToGame,
  removeRooms,
  changePageToRooms,
  renderUsers,
  removeUsers,
  removeReadyButton,
  renderReadyButton,
} from "./domHelpers/game.mjs";
const username = sessionStorage.getItem("username");

if (!username) {
  window.location.replace("/login");
}

const socket = io("/game", { query: { username }, transports: ["websocket"] });
let text = "";
let isGameStarted = false;

socket.on("UPDATE_ROOMS", rooms => {
  const onJoin = room => {
    socket.emit("JOIN_ROOM", room.name, () => {});

    changePageToGame(room.name);
  };

  removeRooms();
  renderRooms(rooms, onJoin);
});

socket.on("UPDATE_USERS", users => {
  removeReadyButton();
  removeUsers();
  renderUsers(users);

  if (!isGameStarted) {
    const user = users.find(user => user.name === username);
    if (user) {
      if (user.isReady) {
        renderReadyButton("NOT READY", () => {
          socket.emit("UPDATE_READY", false);
        });
      } else {
        renderReadyButton("READY", () => {
          socket.emit("UPDATE_READY", true);
        });
      }
    }
  }
});

socket.on("UPDATE_TIMER", timer => {
  removeReadyButton();
  removeBackToRoomsButton();
  removeTimer();

  renderTimer(timer);
});

socket.on("UPDATE_TEXT_ID", textId => {
  GameApi.getText(textId).then(response => {
    text = response.text;
  });
});

const onKeyPress = ({ key }) => {
  socket.emit("KEYPRESS", key, ({ inputtedText, textToInput }) => {
    removeTextContainer();
    renderTextContainer(textToInput, inputtedText);
  });
};

socket.on("GAME_START", () => {
  isGameStarted = true;
  removeTimer();

  renderTextContainer(text);
  document.addEventListener("keypress", onKeyPress);
});

socket.on("GAME_END", comment => {
  updateTimeLeftTimerText("");
  removeTextContainer();
  document.removeEventListener("keypress", onKeyPress);

  renderBackToRoomsButton();
  document.getElementById("quit-room-btn")?.addEventListener("click", backToRoomsButtonOnClick);
  updateComment(comment);
  isGameStarted = false;

  renderReadyButton("READY", () => {
    socket.emit("UPDATE_READY", true);
  });
});

socket.on("UPDATE_TIME_LEFT_TIMER", time => {
  updateTimeLeftTimerText(`${time} seconds left`);
});

socket.on("USERNAME_ALREADY_TAKEN", () => {
  alert("Username is already taken");

  sessionStorage.clear();
  window.location.replace("/login");
});

socket.on("UPDATE_COMMENTATOR_PHRASE", phrase => {
  updateComment(phrase);
});

const addRoomButton = document.getElementById("add-room-button");
addRoomButton.addEventListener("click", () => {
  const roomName = prompt("Enter Room Name:");
  socket.emit("CREATE_ROOM", roomName, response => {
    if (response === "ROOM_ALREADY_EXIST") return alert(`Room name ${roomName} is already taken`);
    else if (response === "INVALID_ROOM_NAME") return alert("Invalid room name");
    else {
      changePageToGame(roomName);
    }
  });
});

const backToRoomsButtonOnClick = () => {
  socket.emit("LEAVE_ROOM", () => {
    changePageToRooms();
  });
};

const backToRoomsButton = document.getElementById("quit-room-btn");
backToRoomsButton.addEventListener("click", backToRoomsButtonOnClick);
